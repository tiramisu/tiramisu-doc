from tiramisu import Config, OptionDescription, IPOption, PortOption, BoolOption, ChoiceOption,DomainnameOption, StrOption, IntOption, Params, ParamOption

def default_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').value.get())
    # False
    print(cfg.option('opt').owner.get())
    # default 

def new_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').owner.get())    
    # default
    cfg.option('opt').value.set(True)
    print(cfg.option('opt').owner.get())
    # user

def back_to_default_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').owner.get())    
    # default
    cfg.option('opt').value.set(True)
    print(cfg.option('opt').owner.get())
    # user
    cfg.option('opt').value.reset()
    print(cfg.option('opt').owner.get())
    # default

def set_option_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').owner.get())    
    # default
    cfg.option('opt').value.set(True)
    print(cfg.option('opt').owner.get())
    # user
    cfg.option('opt').owner.set('owner')
    print(cfg.option('opt').owner.get())
    # owner

def config_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.owner.get())
    # user


def set_owner():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').owner.get())    
    # default
    cfg.owner.set('owner')
    print(cfg.owner.get())
    # owner

def set_owner2():
    opt = BoolOption('opt', '', default= False)
    rootod = OptionDescription('root', '', [opt])
    cfg = Config(rootod)

    print(cfg.option('opt').owner.get())    
    # default
    cfg.owner.set('owner')
    print(cfg.owner.get())
    # owner
    cfg.option('opt').value.set(True)
    print(cfg.option('opt').owner.get())
    # owner


back_to_default_owner()
