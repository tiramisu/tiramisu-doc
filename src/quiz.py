from tiramisu import MetaConfig, Config, OptionDescription, BoolOption, ChoiceOption, StrOption, IntOption, Params, ParamOption, default_storage, list_sessions
from tiramisu.error import ValueOptionError

def verif(q: str, a: str):
    return q == a

def results(v1:bool, v2: bool, v3: bool, v4: bool, v5: bool):
    verif = (v1, v2,v3, v4, v5)
    correct=0
    for v in verif:
        if v == True:
            correct += 1
    return correct


q1 = ChoiceOption('q1', 'Question 1 : What does the cat say ?', ('woof', 'meow'))
a1 = StrOption('a1', 'Answer 1', default='meow', properties=('frozen', ))
v1 = BoolOption('v1', 'Verif of question 1', callback=verif, callback_params=Params((ParamOption(q1), ParamOption(a1)))) 
q2 = ChoiceOption('q2', 'Question 2 : What do you get by mixing blue and yellow ?', ('green', 'red', 'purple'))
a2 = StrOption('a2', 'Answer 2', default='green', properties=('frozen', ))
v2 = BoolOption('v2', 'Verif of question 2', callback=verif, callback_params=Params((ParamOption(q2), ParamOption(a2))))
q3 = ChoiceOption('q3', 'Question 3 : Where is Bryan ?', ('at school', 'in his bedroom', 'in the kitchen'))
a3 = StrOption('a3', 'Answer 3', default='in the kitchen', properties=('frozen', ))
v3 = BoolOption('v3', 'Verif of question 3', callback=verif, callback_params=Params((ParamOption(q3), ParamOption(a3))))
q4 = ChoiceOption('q4', 'Question 4 : Which one has 4 legs and 2 wings ?', ('a wyvern', 'a dragon', 'a wyrm', 'a drake'))
a4 = StrOption('a4', 'Answer 4', default='a dragon', properties=('frozen', ))
v4 = BoolOption('v4', 'Verif of question 4', callback=verif, callback_params=Params((ParamOption(q4), ParamOption(a4))))
q5 = ChoiceOption('q5', 'Question 5 : Why life ?', ('because', 'I don\'t know', 'good question'))
a5 = StrOption('a5', 'Answer 5', default='good question', properties=('frozen', ))
v5 = BoolOption('v5', 'Verif of question 5', callback=verif, callback_params=Params((ParamOption(q5), ParamOption(a5))))

res = IntOption('res', 'Quiz results', callback=results, callback_params=Params((ParamOption(v1), ParamOption(v2), ParamOption(v3), ParamOption(v4), ParamOption(v5))))
rootod = OptionDescription('root', '', [q1, q2, q3, q4, q5, a1, a2, a3, a4, a5, v1, v2, v3, v4, v5, res])
default_storage.setting(engine="sqlite3")
meta_cfg = MetaConfig([], optiondescription=rootod, persistent=True, session_id="quiz")
for session_id in list_sessions():
# our meta config is just here to be a base, so we don't want its session id to be used
    if session_id != "quiz":
            meta_cfg.config.new(session_id, persistent=True)

def run_quiz(meta_cfg: MetaConfig):
    pseudo = input("Enter a name :")
    cfg = meta_cfg.config.new(pseudo, persistent=True)
    cfg.property.read_write()

    nb_q = (len(list(cfg.option.list())) - 1) // 3 + 1

    for qno in range(1, nb_q):
        q = 'q' + str(qno)
        a = 'a' + str(qno)
        v = 'v' + str(qno)
        print(cfg.option(q).information.get('doc'))
        print(*cfg.option(q).value.list(), sep=" | ")
        while True:
            input_ans = input('Your answer : ')
            try:
                cfg.option(q).value.set(input_ans)
            except ValueOptionError as err:
                print(err)
            else:
                break
        print('')
        if cfg.option(v).value.get() is True:
            print("Correct answer !")
            print('')
        else:
            print("Wrong answer...")
            print("The correct answer was : ", cfg.option(a).value.get())
            print('')
                
    print("Correct answers : ", cfg.option('res').value.get(), " out of ", qno)
    if cfg.option('res').value.get() == 0 :
        print("Ouch... Maybe next time ?")
    elif cfg.option('res').value.get() == qno :
        print("Wow, great job !")
    print("==============================================")

    '''''
    Enter a name :DocMaster
    Question 1 : What does the cat say ?
    woof | meow
    Your answer : meow

    Correct answer !

    Question 2 : What do you get by mixing blue and yellow ?
    green | red | purple
    Your answer : green

    Correct answer !

    Question 3 : Where is Bryan ?
    at school | in his bedroom | in the kitchen
    Your answer : in the kitchen

    Correct answer !

    Question 4 : Which one has 4 legs and 2 wings ?
    a wyvern | a dragon | a wyrm | a drake
    Your answer : a dragon

    Correct answer !

    Question 5 : Why life ?
    because | I don't know | good question
    Your answer : good question

    Correct answer !

    Correct answers :  5  out of  5
    Wow, great job !
    ==============================================
        '''''

def quiz_results(meta_cfg: MetaConfig):
    for cfg in meta_cfg.config.list():
        print("==============================================")
        print(cfg.config.name())
        print("")
        nb_q = (len(list(cfg.option.list())) - 1) // 3 + 1

        for qno in range(1, nb_q):
            q = 'q' + str(qno)
            v = 'v' + str(qno)
            print("Question ", qno, " :")
            if (cfg.option(v).value.get()) is True:
                print("Correct answer.")
            else:
                print("Wrong answer :", cfg.option(q).value.get())
            print("")
        print(cfg.config.name(), "'s score : ", cfg.option("res").value.get(), "out of ", qno)
    
    '''''
    DocMaster

    Question  1  :
    Correct answer.

    Question  2  :
    Correct answer.

    Question  3  :
    Correct answer.

    Question  4  :
    Correct answer.

    Question  5  :
    Correct answer.

    DocMaster 's score :  5 out of  5
    '''''
                
run_quiz(meta_cfg)
quiz_results(meta_cfg)