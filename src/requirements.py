from tiramisu import Config, OptionDescription, IPOption, PortOption, BoolOption, ChoiceOption,DomainnameOption, StrOption, Params, ParamOption

def requirement_freeze():
    opt1 = ChoiceOption('opt1', '', ('on', 'off'), default = 'on')
    opt2 = StrOption('opt2', '', requires = [{'option': opt1, 'expected': 'off', 'action':'frozen'}])
    rootod = OptionDescription('root', '', [opt1, opt2])
    cfg = Config(rootod)
    cfg.property.read_write()

    cfg.option('opt1').value.set('off')
    cfg.option('opt2').value.set('value')
    #>>> tiramisu.error.PropertiesOptionError: cannot access to option "opt2" because has property "frozen" (the value of "opt1" is "off")


def requirement_OD() :
    opt1 = ChoiceOption('opt1', '', ('on', 'off'), default = 'on')
    opt2 = StrOption('opt2', '', default = 'value')
    opt3 = BoolOption('opt3', '', default = True)
    optdes1 = OptionDescription('optdes1', '', [opt1])
    optdes2 = OptionDescription('optdes2', '', [opt2, opt3], requires = [{'option': opt1, 'expected': 'off', 'action':'disabled'}])
    rootod = OptionDescription('root', '', [optdes1, optdes2])
    cfg = Config(rootod)
    cfg.property.read_write()

    cfg.option('optdes1.opt1').value.set('off')
    cfg.option('optdes2.opt3').value.set(True)
    #>>> tiramisu.error.PropertiesOptionError: cannot access to optiondescription "optdes2" because has property "disabled" (the value of "opt1" is "off")

def requirement_OD_2():
    opt1 = ChoiceOption('opt1', '', ('on', 'off'), default = 'on')
    opt2 = StrOption('opt2', '', default = 'value')
    opt3 = BoolOption('opt3', '', default = True)
    rootod = OptionDescription('root', '', [opt1, opt2, opt3], requires = [{'option': opt1, 'expected': 'off', 'action':'disabled'}])
    cfg = Config(rootod)
    cfg.property.read_write()

    cfg.option('opt1').value.set('off')
    cfg.option('opt3').value.set(True)
    #No error raised, the OptionDescription hasn't been disabled


def inverse_requirement():
    opt1 = ChoiceOption('opt1', '', ('on', 'off'), default = 'on')
    opt2 = BoolOption('opt2', '', default = True, requires = [{'option': opt1, 'expected': 'on', 'action':'disabled', 'inverse': True}])
    #while opt1's value is 'on', opt2 will not be disabled
    #as soon as opt1's value becomes something else than 'on', opt2 will be disabled
    rootod = OptionDescription('root', '', [opt1, opt2])
    cfg = Config(rootod)
    cfg.property.read_write()

    print(cfg.option('opt2').value.get())
    #opt2 isn't disabled
    cfg.option('opt1').value.set('off')
    print(cfg.option('opt2').value.get())
    #>>> tiramisu.error.PropertiesOptionError: cannot access to option "opt2" because has property "disabled" (the value of "opt1" is not "on")
    #opt2 is now disabled


def transitive_requirement():
    opt1 = ChoiceOption('opt1', '', ('on', 'off'), default = 'on')
    opt2 = ChoiceOption('opt2', '', ('on', 'off'), default = 'on', requires = [{'option': opt1, 'expected': 'off', 'action':'disabled'}])
    opt3 = BoolOption('opt3', '', default = True, requires = [{'option': opt2, 'expected': 'off', 'action':'disabled'}])
    rootod = OptionDescription('root', '', [opt1, opt2, opt3])
    cfg = Config(rootod)
    cfg.property.read_write()

    cfg.option('opt1').value.set('off')
    print(cfg.option('opt3').value.get())
    #>>> tiramisu.error.PropertiesOptionError: cannot access to option "opt2" because has property "disabled" (the value of "opt1" is not "on")
    #opt3 is disabled, because it depends on opt2 which as been disabled






transitive_requirement()
