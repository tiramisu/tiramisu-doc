from tiramisu import Config, OptionDescription, IPOption, PortOption, BoolOption, ChoiceOption,DomainnameOption, StrOption, IntOption, Params, ParamOption

#opt = OptionType('name', 'doc', callback=function_name, callback_params=Params(ParamOption(first_param), ParamOption(second_param), ...))

# First we'll need a function
def double_value(value : int) :
        return value*2

opt1 = IntOption('opt1', '', default=4)
opt2 = IntOption('opt2', '', callback=double_value, callback_params=Params(ParamOption(opt1)))
rootod = OptionDescription('root', '', [opt1, opt2])
cfg = Config(rootod)

# We want opt2 to have the double of opt1's value
print(cfg.option('opt2').value.get())
#>>> 8
# The callback is working !
# But what if we change the value of op1 ?
cfg.option('opt1').value.set(5)
print(cfg.option('opt2').value.get())
#>>> 10
# The value of opt2 changed too !


