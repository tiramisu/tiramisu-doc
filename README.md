# tiramisudoc

tiramisu user documentation

## installation

you need to have docutils and sphinx-doc3 installed.

On Ubuntu, python3-sphinx is a separate package. In my case, I needed to install python3-sphinx:

```
  sudo apt-get install python3-sphinx

```

You can probably run both on a machine, but I just removed the old sphinx-doc:

```

  sudo apt-get remove python-sphinx

```

## sphinx viewcode extension

The source link will not appear if the library is not in the sys.path

conf.py example:

```

  # the code library
  sys.path.insert(0, os.path.abspath(os.path.join('..', 'src')))

```

## sphinx ext extension

The `src` role has been added so that a link on the full source code can appear
like this:

```
    :src:`filename`
```
